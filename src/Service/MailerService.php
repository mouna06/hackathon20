<?php


namespace App\Service;


use App\Entity\Candidature;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }



    public function sendActivationLink ($to, $subject, $template, $options = []) {
        $email = (new TemplatedEmail())
            // from function is mandatory but content will be override by Google account email from environment variable
            ->from('adw.hackathon4@gmail.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($options);

        $this->mailer->send($email);
    }

    public function sendCandidatureLink ($subject, $template, Candidature $candidature) {
        $email = (new TemplatedEmail())
            // from function is mandatory but content will be override by Google account email from environment variable
            ->from('adw.hackathon4@gmail.com')
            ->to('chaibimouna06@gmail.com')
            ->subject($subject)
            ->htmlTemplate($template)
            ->context([
                'candidature' => $candidature
            ]);

        $this->mailer->send($email);
    }



}