<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $canonicalLabel;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="tags")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="tags")
     */
    private $projets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SousTag", mappedBy="tag")
     */
    private $sousTags;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Candidature", mappedBy="tags")
     */
    private $candidatures;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->projets = new ArrayCollection();
        $this->sousTags = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCanonicalLabel(): ?string
    {
        return $this->canonicalLabel;
    }

    public function setCanonicalLabel(string $canonicalLabel): self
    {
        $this->canonicalLabel = $canonicalLabel;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Project $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
        }

        return $this;
    }

    public function removeProjet(Project $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
        }

        return $this;
    }

    /**
     * @return Collection|SousTag[]
     */
    public function getSousTags(): Collection
    {
        return $this->sousTags;
    }

    public function addSousTag(SousTag $sousTag): self
    {
        if (!$this->sousTags->contains($sousTag)) {
            $this->sousTags[] = $sousTag;
            $sousTag->setTag($this);
        }

        return $this;
    }

    public function removeSousTag(SousTag $sousTag): self
    {
        if ($this->sousTags->contains($sousTag)) {
            $this->sousTags->removeElement($sousTag);
            // set the owning side to null (unless already changed)
            if ($sousTag->getTag() === $this) {
                $sousTag->setTag(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->addTag($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            $candidature->removeTag($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->label;
    }
}
