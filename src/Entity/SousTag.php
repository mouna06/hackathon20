<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SousTagRepository")
 */
class SousTag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $canonicalLabel;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="sousTags")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="sousTags")
     */
    private $projets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tag", inversedBy="sousTags")
     */
    private $tag;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Candidature", mappedBy="sousTags")
     */
    private $candidatures;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->projets = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCanonicalLabel(): ?string
    {
        return $this->canonicalLabel;
    }

    public function setCanonicalLabel(string $canonicalLabel): self
    {
        $this->canonicalLabel = $canonicalLabel;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Project $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
        }

        return $this;
    }

    public function removeProjet(Project $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
        }

        return $this;
    }

    public function getTag(): ?Tag
    {
        return $this->tag;
    }

    public function setTag(?Tag $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->addSousTag($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            $candidature->removeSousTag($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->label;
    }
}
