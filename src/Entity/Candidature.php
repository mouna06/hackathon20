<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureRepository")
 */
class Candidature
{

    const GENDER = [
        'homme' => 'm',
        'femme' => 'f',
        'autre' => 'a'
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Expression("this.getLastname() != this.getFirstname()", message="Prénom et nom identique")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actualPoste;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=1)
     * @Assert\Choice(choices=Candidature::GENDER)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "veuillez changer un PDF valide"
     * )
     */
    private $cv;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "veuillez changer un PDF valide"
     * )
     */
    private $portfolio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siteInternet;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $motivation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $CvFilename;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SousTag", inversedBy="candidatures")
     */
    private $sousTags;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="candidatures")
     */
    private $tags;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entreprise;

    public function __construct()
    {
        $this->sousTags = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getActualPoste(): ?string
    {
        return $this->actualPoste;
    }

    public function setActualPoste(string $actualPoste): self
    {
        $this->actualPoste = $actualPoste;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCv(): ?string
    {
        return $this->cv;
    }

    public function setCv(string $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getPortfolio(): ?string
    {
        return $this->portfolio;
    }

    public function setPortfolio(string $portfolio): self
    {
        $this->portfolio = $portfolio;

        return $this;
    }

    public function getSiteInternet(): ?string
    {
        return $this->siteInternet;
    }

    public function setSiteInternet(?string $siteInternet): self
    {
        $this->siteInternet = $siteInternet;

        return $this;
    }

    public function getMotivation(): ?string
    {
        return $this->motivation;
    }

    public function setMotivation(string $motivation): self
    {
        $this->motivation = $motivation;

        return $this;
    }

    public function getCvFilename(): ?string
    {
        return $this->CvFilename;
    }

    public function setCvFilename(string $CvFilename): self
    {
        $this->CvFilename = $CvFilename;

        return $this;
    }

    /**
     * @return Collection|SousTag[]
     */
    public function getSousTags(): Collection
    {
        return $this->sousTags;
    }

    public function addSousTag(SousTag $sousTag): self
    {
        if (!$this->sousTags->contains($sousTag)) {
            $this->sousTags[] = $sousTag;
        }

        return $this;
    }

    public function removeSousTag(SousTag $sousTag): self
    {
        if ($this->sousTags->contains($sousTag)) {
            $this->sousTags->removeElement($sousTag);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function getEntreprise(): ?string
    {
        return $this->entreprise;
    }

    public function setEntreprise(string $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }
}
