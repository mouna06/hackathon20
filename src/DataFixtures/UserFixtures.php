<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\DataFixtures\TagFixtures;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user_1 = new User();

        $user_1->setEmail('user_1@gmail.com');
        $user_1->addTag($this->getReference(TagFixtures::TAG_REFERENCE_1));
        $user_1->setPassword($this->passwordEncoder->encodePassword(
            $user_1,
            'password'
        ));
        $manager->persist($user_1);

        $user_2 = new User();
        $user_2->setEmail('user_2@gmail.com');
        $user_2->addTag($this->getReference(TagFixtures::TAG_REFERENCE_2));
        $user_2->setPassword($this->passwordEncoder->encodePassword(
            $user_2,
            'password'
        ));
        $manager->persist($user_2);

        $user_3 = new User();
        $user_3->setEmail('user_3@gmail.com');
        $user_3->addTag($this->getReference(TagFixtures::TAG_REFERENCE_3));
        $user_3->setPassword($this->passwordEncoder->encodePassword(
            $user_3,
            'password'
        ));
        $manager->persist($user_3);

        $user_4 = new User();
        $user_4->setEmail('user_4@gmail.com');
        $user_4->addTag($this->getReference(TagFixtures::TAG_REFERENCE_4));
        $user_4->setPassword($this->passwordEncoder->encodePassword(
            $user_4,
            'password'
        ));
        $manager->persist($user_4);

        $user_5 = new User();
        $user_5->setEmail('user_5@gmail.com');
        $user_5->addTag($this->getReference(TagFixtures::TAG_REFERENCE_5));
        $user_5->setPassword($this->passwordEncoder->encodePassword(
            $user_5,
            'password'
        ));
        $manager->persist($user_5);

        $user_6 = new User();
        $user_6->setEmail('user_6@gmail.com');
        $user_6->addTag($this->getReference(TagFixtures::TAG_REFERENCE_6));
        $user_6->setPassword($this->passwordEncoder->encodePassword(
            $user_6,
            'password'
        ));
        $manager->persist($user_6);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            TagFixtures::class,
        );
    }
}
