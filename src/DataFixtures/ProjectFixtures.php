<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $projet_1 = new Project();
        $projet_1->setLabel('Project_1');
        $projet_1->setDescription('Très longue description du Projet_1');
        $projet_1->addTag($this->getReference(TagFixtures::TAG_REFERENCE_1));
        $projet_1->setCost(3000);
        $manager->persist($projet_1);

        $projet_2 = new Project();
        $projet_2->setLabel('Project_2');
        $projet_2->setDescription('Très longue description du Projet_2');
        $projet_2->addTag($this->getReference(TagFixtures::TAG_REFERENCE_2));
        $projet_2->addTag($this->getReference(TagFixtures::TAG_REFERENCE_3));
        $projet_2->setCost(3000);
        $manager->persist($projet_2);

        $projet_3 = new Project();
        $projet_3->setLabel('Project_3');
        $projet_3->setDescription('Très longue description du Projet_3');
        $projet_3->addTag($this->getReference(TagFixtures::TAG_REFERENCE_3));
        $projet_3->setCost(3000);
        $manager->persist($projet_3);

        $projet_4 = new Project();
        $projet_4->setLabel('Project_4');
        $projet_4->setDescription('Très longue description du Projet_4');
        $projet_4->addTag($this->getReference(TagFixtures::TAG_REFERENCE_4));
        $projet_4->setCost(3000);
        $manager->persist($projet_4);

        $projet_5 = new Project();
        $projet_5->setLabel('Project_5');
        $projet_5->setDescription('Très longue description du Projet_5');
        $projet_5->addTag($this->getReference(TagFixtures::TAG_REFERENCE_5));
        $projet_5->setCost(3000);
        $manager->persist($projet_5);

        $projet_6 = new Project();
        $projet_6->setLabel('Project_6');
        $projet_6->setDescription('Très longue description du Projet_6');
        $projet_6->addTag($this->getReference(TagFixtures::TAG_REFERENCE_6));
        $projet_6->setCost(3000);
        $manager->persist($projet_6);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            TagFixtures::class,
        );
    }
}
