<?php


namespace App\DataFixtures;


use App\Entity\SousTag;
use Doctrine\Bundle\FixturesBundle\Fixture;
Use Doctrine\Common\Persistence\ObjectManager;

class SousTagFixtures extends Fixture
{
    public const SOUSTAG_REFERENCE_1 = 'soustag-1';
    public const SOUSTAG_REFERENCE_2 = 'soustag-2';
    public const SOUSTAG_REFERENCE_3 = 'soustag-3';
    public const SOUSTAG_REFERENCE_4 = 'soustag-4';
    public const SOUSTAG_REFERENCE_5 = 'soustag-5';
    public const SOUSTAG_REFERENCE_6 = 'soustag-6';
    public const SOUSTAG_REFERENCE_7 = 'soustag-7';
    public const SOUSTAG_REFERENCE_8 = 'soustag-8';
    public const SOUSTAG_REFERENCE_9 = 'soustag-9';
    public const SOUSTAG_REFERENCE_10 = 'soustag-10';
    public const SOUSTAG_REFERENCE_11 = 'soustag-11';
    public const SOUSTAG_REFERENCE_12 = 'soustag-12';
    public const SOUSTAG_REFERENCE_13 = 'soustag-13';
    public const SOUSTAG_REFERENCE_14 = 'soustag-14';
    public const SOUSTAG_REFERENCE_15 = 'soustag-15';
    public const SOUSTAG_REFERENCE_16 = 'soustag-16';
    public const SOUSTAG_REFERENCE_17 = 'soustag-17';
    public const SOUSTAG_REFERENCE_18 = 'soustag-18';
    public const SOUSTAG_REFERENCE_19 = 'soustag-19';
    public const SOUSTAG_REFERENCE_20 = 'soustag-20';
    public const SOUSTAG_REFERENCE_21 = 'soustag-21';
    public const SOUSTAG_REFERENCE_22 = 'soustag-22';
    public const SOUSTAG_REFERENCE_23 = 'soustag-23';
    public const SOUSTAG_REFERENCE_24 = 'soustag-24';
    public const SOUSTAG_REFERENCE_25 = 'soustag-25';
    public const SOUSTAG_REFERENCE_26 = 'soustag-26';
    public const SOUSTAG_REFERENCE_27 = 'soustag-27';
    public const SOUSTAG_REFERENCE_28 = 'soustag-28';
    public const SOUSTAG_REFERENCE_29 = 'soustag-29';
    public const SOUSTAG_REFERENCE_30 = 'soustag-30';
    public const SOUSTAG_REFERENCE_31 = 'soustag-31';
    public const SOUSTAG_REFERENCE_32 = 'soustag-32';
    public const SOUSTAG_REFERENCE_33 = 'soustag-33';
    public const SOUSTAG_REFERENCE_34 = 'soustag-34';
    public const SOUSTAG_REFERENCE_35 = 'soustag-35';
    public const SOUSTAG_REFERENCE_36 = 'soustag-36';



    public function load(ObjectManager $manager)
    {
        $soustag_1 = new SousTag();
        $soustag_1->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_1->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_1);

        $soustag_2 = new SousTag();
        $soustag_2->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_2->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_2);


        $soustag_3 = new SousTag();
        $soustag_3->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_3->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_3);

        $soustag_4 = new SousTag();
        $soustag_4->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_4->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_4);


        $soustag_5 = new SousTag();
        $soustag_5->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_5->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_5);

        $soustag_6 = new SousTag();
        $soustag_6->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_6->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_6);


        $soustag_7 = new SousTag();
        $soustag_7->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_7->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_7);

        $soustag_8 = new SousTag();
        $soustag_8->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_8->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_8);

        $soustag_9 = new SousTag();
        $soustag_9->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_9->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_9);

        $soustag_10 = new SousTag();
        $soustag_10->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_10->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_10);


        $soustag_11 = new SousTag();
        $soustag_11->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_11->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_11);

        $soustag_12 = new SousTag();
        $soustag_12->setLabel('POSITIONNEMENT & STRATÉGIE');
        $soustag_12->setCanonicalLabel('positionnement et strategie');
        $manager->persist($soustag_12);


        $manager->flush();

        $this->addReference(self::SOUSTAG_REFERENCE_1, $soustag_1);
        $this->addReference(self::SOUSTAG_REFERENCE_2, $soustag_2);
        $this->addReference(self::SOUSTAG_REFERENCE_3, $soustag_3);
        $this->addReference(self::SOUSTAG_REFERENCE_4, $soustag_4);
        $this->addReference(self::SOUSTAG_REFERENCE_5, $soustag_5);
        $this->addReference(self::SOUSTAG_REFERENCE_6, $soustag_6);
        $this->addReference(self::SOUSTAG_REFERENCE_7, $soustag_7);
        $this->addReference(self::SOUSTAG_REFERENCE_8, $soustag_8);
        $this->addReference(self::SOUSTAG_REFERENCE_9, $soustag_9);
        $this->addReference(self::SOUSTAG_REFERENCE_10, $soustag_10);
        $this->addReference(self::SOUSTAG_REFERENCE_11, $soustag_11);
        $this->addReference(self::SOUSTAG_REFERENCE_12, $soustag_12);
    }
}