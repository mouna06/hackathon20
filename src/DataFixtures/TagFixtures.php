<?php


namespace App\DataFixtures;


use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
Use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public const TAG_REFERENCE_1 = 'tag-1';
    public const TAG_REFERENCE_2 = 'tag-2';
    public const TAG_REFERENCE_3 = 'tag-3';
    public const TAG_REFERENCE_4 = 'tag-4';
    public const TAG_REFERENCE_5 = 'tag-5';
    public const TAG_REFERENCE_6 = 'tag-6';

    public function load(ObjectManager $manager)
    {
        $tag_1 = new Tag();
        $tag_1->setLabel('Conseil');
        $tag_1->setCanonicalLabel('conseil');
        $tag_1->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_1));
        $tag_1->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_2));

        $manager->persist($tag_1);

        $tag_2 = new Tag();
        $tag_2->setLabel('Création');
        $tag_2->setCanonicalLabel('creation');
        $tag_2->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_3));
        $tag_2->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_4));
        $manager->persist($tag_2);

        $tag_3 = new Tag();
        $tag_3->setLabel('Application');
        $tag_3->setCanonicalLabel('application');
        $tag_3->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_5));
        $tag_3->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_6));
        $manager->persist($tag_3);

        $tag_4 = new Tag();
        $tag_4->setLabel('Web');
        $tag_4->setCanonicalLabel('web');
        $tag_4->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_7));
        $tag_4->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_8));
        $manager->persist($tag_4);

        $tag_5 = new Tag();
        $tag_5->setLabel('Event');
        $tag_5->setCanonicalLabel('event');
        $tag_5->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_9));
        $tag_5->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_10));
        $manager->persist($tag_5);

        $tag_6 = new Tag();
        $tag_6->setLabel('Buzz');
        $tag_6->setCanonicalLabel('buzz');
        $tag_6->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_11));
        $tag_6->addSousTag($this->getReference(SousTagFixtures::SOUSTAG_REFERENCE_12));
        $manager->persist($tag_6);

        $manager->flush();

        $this->addReference(self::TAG_REFERENCE_1, $tag_1);
        $this->addReference(self::TAG_REFERENCE_2, $tag_2);
        $this->addReference(self::TAG_REFERENCE_3, $tag_3);
        $this->addReference(self::TAG_REFERENCE_4, $tag_4);
        $this->addReference(self::TAG_REFERENCE_5, $tag_5);
        $this->addReference(self::TAG_REFERENCE_6, $tag_6);
    }
}