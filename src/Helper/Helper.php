<?php

namespace App\Helper;

class Helper
{
    /**
     * @param null|int $size
     * @return string
     */
    static function generateUniqueId(int $size = 50): string
    {
        $id = md5(''.(time() * rand(100, 999)));

        if (!is_null($size) && is_int($size)) {
            $id = substr($id, 0, $size);
        }

        return $id;
    }

    static function generateToken(int $size = 30): string
    {
        $id = md5(''.(time() * rand(100, 999)));

        if (!is_null($size) && is_int($size)) {
            $id = substr($id, 0, $size);
        }

        return $id;
    }

    static function generateUserPassword(int $size = 10): string
    {
        $id = md5(''.(time() * rand(100, 999)));

        if (!is_null($size) && is_int($size)) {
            $id = substr($id, 0, $size);
        }

        return $id;
    }

}
