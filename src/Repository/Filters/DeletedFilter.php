<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-10-31
 * Time: 15:17
 */

namespace App\Repository\Filters;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class DeletedFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->hasField("deletedAt")){
            $date = date("Y-m-d H:i:s");

            return $targetTableAlias.".deleted_at IS NULL";
        }

        return "";
    }
}