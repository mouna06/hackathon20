<?php

namespace App\Controller;

use App\Entity\Candidature;
use App\Entity\User;
use App\Helper\Helper;
use App\Repository\CandidatureRepository;
use App\Repository\ProjectRepository;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/candidatures")
 */
class AdminCandidaturesController extends AbstractController
{
    /**
     * @Route("/", name="admin_candidatures", methods={"GET"})
     */
    public function index(CandidatureRepository $candidatureRepository): Response
    {
        return $this->render('back/admin_candidatures/index.html.twig', [
            'candidatures' => $candidatureRepository->findAll(),
        ]);
    }


    /**
     * @Route("/{id}", name="admin_candidature_show", methods={"GET"})
     */
    public function show(Candidature $candidature): Response
    {
        return $this->render('back/admin_candidatures/show.html.twig', [
            'candidature' => $candidature,
        ]);
    }



    /**
     * @Route("/accepted/{id}", name="candidature_accepted")
     * @param Candidature $candidature
     * @return Response
     */


    public function accept(Candidature $candidature, UserPasswordEncoderInterface $passwordEncoder, MailerService $mailerService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)->findAll();
        dump($users);

        $user = new User();
        $password = Helper::generateUserPassword();

        $user->setEmail($candidature->getEmail());
        $user->setPassword($passwordEncoder->encodePassword($user, $password));

        $options['password'] = $password;
        $mailerService->sendActivationLink($user->getEmail(), 'Connectez vous à votre compte chez 148', 'email/login_link.html.twig', $options);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();


        return $this->redirectToRoute('home');



    }
}
