<?php

namespace App\Controller;

use App\Entity\Candidature;
use App\Form\CandidatureType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CandidatureController extends AbstractController
{
    /**
     * @Route("/candidature/{id}", name="candidature_show")
     * @param Candidature $candidature
     * @return Response
     */
    public function show(Candidature $candidature)
    {
        if (!$candidature) {
            throw $this->createNotFoundException();
        }

        return $this->render('back/candidature/show.html.twig', [
            'candidature' => $candidature
        ]);
    }


    /**
     * @Route("/delete/{id}", name="candidature_delete")
     * @param Candidature $candidature
     * @return Response
     */
    public function delete(Candidature $candidature)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if($candidature != null)
        {
            $entityManager->remove($candidature);
            $entityManager->flush();
        }
        return $this->redirectToRoute('home');


    }
    /**
     * @Route("/candidature", name="candidature")
     *
     */
    public function index(Request $request, MailerService $mailerService)
    {

        $candidature = new Candidature();
        $form = $this->createForm(CandidatureType::class, $candidature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

     /*       $cvFile = $form['cv']->getData();
            if ($cvFile) {
                $cvFileName = $fileUploader->upload($cvFile);
                $candidature->setCvFilename($cvFileName);
            }
*/

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($candidature);
            $entityManager->flush();
            $mailerService->sendCandidatureLink('Nouvelle Candidature', 'email/candidature_link.html.twig', $candidature);


            return $this->redirectToRoute('home');
        }

        return $this->render('front/candidature/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}
