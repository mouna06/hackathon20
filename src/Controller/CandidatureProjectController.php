<?php

namespace App\Controller;

use App\Entity\CandidatureProject;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class CandidatureProjectController extends AbstractController
{
    /**
     * @Route("/candidature/projet", name="candidature_project")
     */
    public function index()
    {
        $candidatureProjects = $this->getDoctrine()->getRepository(CandidatureProject::class)->findAll();

        return $this->render('back/candidature_project/index.html.twig', [
            'candidatures' => $candidatureProjects,
        ]);
    }

    /**
     * @Route("/candidature/project/{id}", name="candidature_project_show")
     * @param CandidatureProject $candidatureProject
     * @return Response
     */
    public function show(CandidatureProject $candidatureProject)
    {
        return $this->render('back/candidature_project/show.html.twig', [
            'candidatureProject' => $candidatureProject,
        ]);
    }

    /**
     * @Route("/candidature/project/{id}/new", name="candidature_project_new")
     * @param Project $project
     * @return RedirectResponse
     */
    public function new(Project $project)
    {
        /** @var User $user */
        $user = $this->getUser();

        $candidatureProject = new CandidatureProject();

        $candidatureProject->setUserCandidature($user);
        $candidatureProject->setProject($project);

        $em = $this->getDoctrine()->getManager();
        $em->persist($candidatureProject);
        $em->flush();

        $this->addFlash('success', 'La candidature a bien été envoyé');
        return $this->redirectToRoute('project_index', [
            'projects' => $this->getDoctrine()->getRepository(Project::class)->findAll(),
        ]);
    }

    /**
     * @Route("/candidature/project/{id}/validate-{validate}", name="candidature_project_validate")
     * @param CandidatureProject $candidatureProject
     * @param string $validate
     * @return Response
     */
    public function validate(CandidatureProject $candidatureProject, string $validate)
    {
        if ($validate === 'true') {
            $validate = true;
        } else {
            $validate = false;
        }
        $candidatureProject->setIsValidated($validate);

        $this->getDoctrine()->getManager()->flush();

        return $this->render('back/candidature_project/show.html.twig', [
            'candidatureProject' => $candidatureProject,
        ]);
    }
}
